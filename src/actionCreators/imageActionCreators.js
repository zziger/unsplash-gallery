/*
 * @param {number} [page] */
export function getImages(page) {
  return {
    type: 'image/GET_IMAGES',
    payload: page,
  };
}

export function getImagesRequest() {
  return {
    type: 'image/GET_IMAGES_REQUEST',
  };
}

export function getImagesSuccess(images) {
  return {
    type: 'image/GET_IMAGES_SUCCESS',
    payload: images,
  };
}

export function getImagesFailure(error) {
  return {
    type: 'image/GET_IMAGES_FAILURE',
    payload: error,
  };
}
