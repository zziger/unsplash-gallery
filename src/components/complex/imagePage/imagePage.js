import * as React from 'react';
import {Image, StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  test: {
    width: '100%',
    height: '100%',
  },
});

export default class ImagePage extends React.Component {
  render() {
    const image = this.props.images.find(
      (i) => i.id === this.props.route.params.id,
    );
    if (!image || !image.urls || !image.urls.full) {
      this.props.navigation.goBack();
    }

    return (
      <>
        <Image
          style={styles.test}
          source={{
            uri: image.urls.full,
          }}
        />
      </>
    );
  }
}
