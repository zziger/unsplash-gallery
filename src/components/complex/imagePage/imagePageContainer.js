import {connect} from 'react-redux';
import ImagePage from './imagePage';

const mapStateToProps = (state) => {
  return {
    images: state.images.images,
  };
};

const ImagePageContainer = connect(mapStateToProps, null)(ImagePage);
export default ImagePageContainer;
