import * as React from 'react';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableHighlight,
  View,
} from 'react-native';

const styles = StyleSheet.create({
  container: {
    paddingTop: 20,
  },
  block: {
    marginHorizontal: 5,
    height: 'auto',
    flex: 1,
    flexDirection: 'column',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
    marginBottom: 5,
    backgroundColor: 'white',
    borderRadius: 10,
    overflow: 'hidden',
  },
  rightBlock: {
    flexGrow: 1,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    padding: 20,
  },
  image: {
    width: '100%',
    height: 200,
  },
  title: {
    fontSize: 20,
    marginBottom: 3,
  },
  author: {
    color: '#999999',
  },
});

export default class ImagesPage extends React.Component {
  componentDidMount() {
    if (!this.props.images.length) {
      this.fetchImages();
    }
  }

  fetchImages() {
    if (!this.props.isLoading) {
      this.props.getImages();
    }
  }

  render() {
    return (
      <>
        <FlatList
          data={this.props.images}
          style={styles.container}
          renderItem={({item: image}) => (
            <TouchableHighlight
              underlayColor="#DDDDDD"
              onPress={() =>
                this.props.navigation.navigate('Image', {id: image.id})
              }
              style={styles.block}>
              <View>
                <Image source={{uri: image.urls.small}} style={styles.image} />
                <View style={styles.rightBlock}>
                  {(image.description || image.altDescription) && (
                    <Text numberOfLines={1} style={styles.title}>
                      {image.description || image.altDescription}
                    </Text>
                  )}
                  <View>
                    {!(image.description || image.altDescription) && (
                      <Text>Author: </Text>
                    )}
                    <Text style={styles.author}>{image.user.name}</Text>
                  </View>
                </View>
              </View>
            </TouchableHighlight>
          )}
          keyExtractor={(item, index) => item.id + index}
          onEndReached={this.fetchImages.bind(this)}
          onEndReachedThreshold={10}
        />
      </>
    );
  }
}
