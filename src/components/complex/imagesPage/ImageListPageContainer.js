import {connect} from 'react-redux';
import ImagesPage from './imagesPage';
import {getImages} from '../../../actionCreators/imageActionCreators';

const mapStateToProps = (state) => {
  return {
    images: state.images.images,
    isLoading: state.isLoading['image/GET_IMAGES'],
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getImages() {
      dispatch(getImages());
    },
  };
};

const ImagesPageContainer = connect(
  mapStateToProps,
  mapDispatchToProps,
)(ImagesPage);
export default ImagesPageContainer;
