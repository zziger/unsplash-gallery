const initialState = {
  images: [],
  page: 1,
};

export default function imageReducer(state = initialState, action) {
  switch (action.type) {
    case 'image/GET_IMAGES_SUCCESS':
    case 'image/ADD_IMAGES':
      return {
        images: [...state.images, ...action.payload],
        page: state.page + 1,
      };

    default:
      return state;
  }
}
