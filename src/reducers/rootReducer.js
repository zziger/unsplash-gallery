import imageReducer from './imageReducer';
import errorReducer from './errorReducer';
import isLoadingReducer from './isLoadingReducer';
import {combineReducers} from 'redux';

const rootReducer = combineReducers({
  images: imageReducer,
  error: errorReducer,
  isLoading: isLoadingReducer,
});

export default rootReducer;
