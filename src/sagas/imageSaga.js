// import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
// import * as actionCreators from '../actionCreators/authActionCreators';
// import * as actionTypes from '../actionTypes/authActionTypes';
// import API from '../services/API';
//
// namespace AuthSaga {
//   export function* onAuthorize({ login, password, rememberMe }: actionTypes.AuthorizeAction): any {
//     try {
//       yield put(actionCreators.authorizeRequest());
//       const token = yield call(API.authorize, login, password, rememberMe);
//       yield put(actionCreators.authorizeSuccess(token));
//     } catch (error) {
//       yield put(actionCreators.authorizeFailure(error.message));
//     }
//   }
//
//   export function* onRefresh(): any {
//     try {
//       const token = yield call(API.refreshToken);
//       yield put(actionCreators.setToken(token || ''));
//     } catch (error) {
//       yield put(actionCreators.logOut());
//     }
//   }
//
//   export function* watchOnLoadLyrics(): any {
//     yield takeEvery(actionTypes.AUTHORIZE, AuthSaga.onAuthorize);
//     yield takeEvery(actionTypes.REFRESH_TOKEN, AuthSaga.onRefresh);
//   }
//
//   export function* saga(): any {
//     yield all([fork(AuthSaga.watchOnLoadLyrics)]);
//   }
// }
//
// export default AuthSaga;

import {
  getImagesFailure,
  getImagesRequest,
  getImagesSuccess,
} from '../actionCreators/imageActionCreators';
import {getImagesFromAPI} from '../services/UnsplashAPI';
import {select, put, takeEvery, all, fork, call} from 'redux-saga/effects';

function* onGetImages({payload}) {
  try {
    yield put(getImagesRequest());
    const state = yield select();
    const data = yield call(getImagesFromAPI, (state.images.page || 1));
    console.log(data.data);
    if (!Array.isArray(data.data)) {
      throw new Error('wrong formatted data');
    }
    yield put(getImagesSuccess(data.data));
  } catch (e) {
    yield put(getImagesFailure(e));
  }
}

function* watchImages() {
  yield takeEvery('image/GET_IMAGES', onGetImages);
}

export function* saga() {
  yield all([fork(watchImages)]);
}
