import {all, fork} from 'redux-saga/effects';
import {saga as ImageSaga} from './imageSaga';

export default function* rootSaga() {
  yield all([fork(ImageSaga)]);
}
