import * as axios from 'axios';

const clientID =
  '***REMOVED***';

export async function getImagesFromAPI(page = 1) {
  return await axios.get(
    `https://api.unsplash.com/photos/?client_id=${clientID}&page=${page}`,
  );
}
