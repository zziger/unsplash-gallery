import {applyMiddleware, createStore} from 'redux';
import createSagaMiddleware from 'redux-saga';
import rootReducer from '../reducers/rootReducer';
import rootSaga from '../sagas/rootSaga';

const sagaMiddleware = createSagaMiddleware();
const middlewares = [sagaMiddleware];

if (__DEV__) {
  middlewares.push(require('redux-logger').default);
}

const store = createStore(rootReducer, applyMiddleware(...middlewares));
// noinspection JSUnresolvedFunction
sagaMiddleware.run(rootSaga);

export default store;
